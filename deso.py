import pyautogui as pag
import time
import pyperclip

# Define the coordinates and use the `actions` list
actions = [
    (904, 68, 2),  # minizim
    (904, 68, 2),  # minizim
    (111, 135, 2),  # rightclick
    (154, 150, 2),  # ok
    (675, 101, 1),  # rightclick
    (396, 101, 1),  # open
]

# Wait for a few seconds to give time to focus on the target application
time.sleep(2)

# Perform the actions in the specified order
for x, y, duration in actions:
    if (x, y) == (111, 135) or (x, y) == (675, 435):
        # For right-click coordinates, perform right-click
        pag.rightClick(x, y, duration=duration)
    else:
        pag.click(x, y, duration=duration)
    
print("Done , Log in Credintials is below")
