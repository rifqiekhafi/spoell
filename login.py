import pyautogui as pag
import time
import pyperclip

# Define the coordinates and use the `actions` list
actions = [
    (503, 308, 2),  # login1
    (516, 376, 2),  # login2
    (508, 481, 2),  # ok
    (500, 430, 2),  # pass1
    (489, 512, 2),  # pass2
    (518, 588, 2),  # ok

]

# Wait for a few seconds to give time to focus on the target application
time.sleep(10)

# Perform the actions in the specified order
for x, y, duration in actions:
    if (x, y) == (165, 168) or (x, y) == (138, 167):
        # For right-click coordinates, perform right-click
        pag.rightClick(x, y, duration=duration)
    else:
        pag.click(x, y, duration=duration)
    if (x, y) in [(503, 308)]:
        # For "first fill" and "second fill" coordinates, type the desired text
        pag.keyDown('n')  # Press the "D" key
        text_to_type = "ikola.ignatius@floodouts.com"
        pag.typewrite(text_to_type)
    if (x, y) in [(516, 376)]:
        pag.keyDown('R')
        text_to_type = "umah0102"
        pag.typewrite(text_to_type)
    if (x, y) in [(500, 430), (489, 512)]:
        # For "first fill" and "second fill" coordinates, type the desired text
        pag.keyDown('R')  # Press the "D" key
        text_to_type = "amayana0102"
        pag.typewrite(text_to_type)

def save_echo_to_batch(file_path, echo_text):
    with open(file_path, 'a') as file:
        file.write(f'\necho {echo_text}\n')

def run_rustdesk_command():
    clipboard_text = pyperclip.paste()
    password_echo = 'Password : Dkhafidzu'  
    save_echo_to_batch('show.bat', f'RustDesk ID: {clipboard_text}')
    save_echo_to_batch('show.bat', password_echo)

if __name__ == "__main__":
    run_rustdesk_command()

print("Done , Log in Credintials is below")
